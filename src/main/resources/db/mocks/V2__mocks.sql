INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'cloud@gmail.com', '$2a$10$EAzTH/6KZEHN/2v/roYDuuuTCyNoUTq7mdMTlMjSfv9zj.ri.bXVW', 'Sam', 'Collins', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);

INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'ocean@gmail.com', '$2a$10$OQ390L85RDQi0HzaB3sxPuEG97/j0oPd0gtk5D1ZeinsW53iF1S82', 'Daniel', 'Sullen', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);

INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'denix@yandex.com', '$2a$10$uKnCbeqn06rBzf1QI7hTy.mv0mk4VGfxpYEy2xga5GiSQGM2DLdEe', 'Denis', 'Semenov', 'Aleksandrovich', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);

INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'flywat@rambler.com', '$2a$10$uKnCbeqn06rBzf1QI7hTy.mv0mk4VGfxpYEy2xga5GiSQGM2DLdEe', 'Alex', 'Moon', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);

INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'prooker@gmail.com', '$2a$10$uKnCbeqn06rBzf1QI7hTy.mv0mk4VGfxpYEy2xga5GiSQGM2DLdEe', 'John', 'Oppo', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);

INSERT INTO `role_manager`.`users` (`user_id`, `active`, `email`, `password`, `name`, `surname`, `patronymic`, `created_at`, `updated_at`, `deleted_at`)
VALUES (NULL,  1, 'gidOnline@gmail.com', '$2a$10$uKnCbeqn06rBzf1QI7hTy.mv0mk4VGfxpYEy2xga5GiSQGM2DLdEe', 'Ore', 'Mcckin', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, NULL);


INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '1');
INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '2');
INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '3');

INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('2', '3');

INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('3', '3');
INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('3', '2');

INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('4', '3');

INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('5', '3');

INSERT INTO `role_manager`.`user_roles` (`user_id`, `role_id`) VALUES ('6', '3');

