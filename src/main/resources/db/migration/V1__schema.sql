CREATE TABLE users (
  user_id    BIGINT(20)                           NOT NULL AUTO_INCREMENT,
  active     BIGINT(20)                           NOT NULL,
  email      VARCHAR(255)                         NOT NULL UNIQUE,
  password   VARCHAR(255)                         NOT NULL,
  name       VARCHAR(255)                         NOT NULL,
  surname    VARCHAR(255)                         NOT NULL,
  patronymic VARCHAR(255)                         NULL,
  created_at DATETIME                             NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP NULL     DEFAULT CURRENT_TIMESTAMP,
  deleted_at DATETIME                             NULL     DEFAULT NULL,
  PRIMARY KEY (user_id)
);
CREATE TABLE roles (
  role_id BIGINT(20)  NOT NULL AUTO_INCREMENT,
  role    VARCHAR(45) NOT NULL UNIQUE,
  PRIMARY KEY (role_id)
);
CREATE TABLE user_roles (
  user_id BIGINT(20) NOT NULL,
  role_id BIGINT(20) NOT NULL,
  UNIQUE INDEX UNIQUE_ROLE (user_id ASC, role_id ASC),
  INDEX FK_role_idx (role_id ASC),
  CONSTRAINT FK_user FOREIGN KEY (user_id) REFERENCES users (user_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT FK_role FOREIGN KEY (role_id) REFERENCES roles (role_id)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
);


INSERT INTO roles (role) VALUES ('ADMIN');
INSERT INTO roles (role) VALUES ('DEVELOPER');
INSERT INTO roles (role) VALUES ('USER');