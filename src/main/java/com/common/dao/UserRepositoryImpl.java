package com.common.dao;

import com.common.model.User;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User findById(long id) {
		Query query = entityManager.createNativeQuery(
				"SELECT u.* FROM users AS u WHERE u.user_id = (?1)", User.class);
		query.setParameter(1, id);
		List list = query.getResultList();
		return (list.size() > 0) ? (User) list.get(0) : null;
	}

	@Override
	public List<User> findUsersByRole(long roleId) {
		Query query = entityManager.createNativeQuery(
				"SELECT u.* FROM user_roles AS ur LEFT JOIN users AS u ON ur.user_id = u.user_id WHERE ur.role_id = ?1",
				User.class);
		query.setParameter(1, roleId);
		//noinspection unchecked
		return query.getResultList();
	}

	@Override
	public void deleteUser(long id) {
		Query query = entityManager.createNativeQuery("DELETE FROM users WHERE user_id = ?1");
		query.setParameter(1, id);
		query.executeUpdate();
	}
}
