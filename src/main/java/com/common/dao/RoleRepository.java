package com.common.dao;

import com.common.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>, RoleRepositoryCustom {
	Role findByRole(String nameRole);
}
