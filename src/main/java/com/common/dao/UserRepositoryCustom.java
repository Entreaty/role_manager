package com.common.dao;

import com.common.model.User;

import java.util.List;

interface UserRepositoryCustom {

	User findById(long id);

	List<User> findUsersByRole(long roleId);

	void deleteUser(long id);

}
