package com.common.dao;

import com.common.model.Role;

interface RoleRepositoryCustom {
	Role findByRoleName(String roleName);
}
