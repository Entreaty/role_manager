package com.common.dao;

import com.common.model.Role;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class RoleRepositoryImpl implements RoleRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Cacheable("role")
	public Role findByRoleName(String roleName) {
		Query query = entityManager.createNativeQuery(
				"SELECT r.* FROM roles AS r WHERE r.role = (?1)", Role.class);
		query.setParameter(1, roleName);
		List list = query.getResultList();
		return (list.size() > 0) ? (Role) list.get(0) : null;
	}
}
