package com.common;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@WebController
public class HtmlController {

	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("redirect:/swagger-ui.html");
	}

}
