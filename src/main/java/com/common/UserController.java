package com.common;

import com.common.dao.RoleRepository;
import com.common.dao.UserRepository;
import com.common.exception.ApiException;
import com.common.model.Role;
import com.common.model.User;
import com.common.service.CommonService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/user")
@Validated
public class UserController {

	@Autowired
	private CommonService commonService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@ApiOperation(value = "Create user", notes = "Default password is 12345. " +
			"For assign multiple roles provide name of roles in new lines.")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public User create(
			@RequestParam(value = "roles[]", defaultValue = "", required = false) String[] roles,
			@RequestParam(value = "password", defaultValue = "12345") String password,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "patronymic", required = false) String patronymic) {
		commonService.checkAndSaveRoles(roles);
		User user = new User();
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setPatronymic(patronymic);
		user.setPassword(bCryptPasswordEncoder.encode(password));
		user.setActive(1);
		user.setRoles(getRoles(Arrays.asList(roles)));
		userRepository.saveAndFlush(user);
		return user;
	}
	@ApiOperation(value = "Update user", notes = "For assign multiple roles provide name of roles in new lines.")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@Transactional
	public User update(@RequestParam(value = "roles[]", defaultValue = "", required = false) String[] roles,
					   @RequestParam(defaultValue = "", required = false) String password,
					   @PathVariable("id") long id,
					   @RequestParam(value = "name", defaultValue = "", required = false) String name,
					   @RequestParam(value = "surname", defaultValue = "", required = false) String surname,
					   @RequestParam(value = "email", defaultValue = "", required = false) String email,
					   @RequestParam(value = "patronymic", required = false) String patronymic) {
		User user = userRepository.findById(id);
		commonService.checkUser(user);
		commonService.checkAndSaveRoles(roles);
		if (!email.equals("")) user.setEmail(email);
		if (!name.equals("")) user.setName(name);
		if (!surname.equals("")) user.setSurname(surname);
		user.setPatronymic(patronymic);
		if (!password.equals("")) user.setPassword(bCryptPasswordEncoder.encode(password));
		user.setRoles(getRoles(Arrays.asList(roles)));
		userRepository.saveAndFlush(user);
		return user;
	}
	@ApiOperation(value = "Delete user")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("id") @NotNull long id) {
		userRepository.deleteUser(id);
	}

	@ApiOperation(value = "Get user by ID", notes = "Returns a single user, if it exists")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public User getOne(@PathVariable("id") long id) {
		return userRepository.findById(id);
	}

	@ApiOperation(value = "Get users by role", notes = "Returns users with a role")
	@RequestMapping(value = "/users-by-role", method = RequestMethod.GET)
	@ResponseBody
	public List<User> getListByRole(@RequestParam("role") String role) {
		Role roleExist = roleRepository.findByRoleName(role);
		if (roleExist == null) throw new ApiException("Bad role name");
		return userRepository.findUsersByRole(roleExist.getId());
	}

	private Set<Role> getRoles(List<String> roles) {
		Set<Role> userRoles = new HashSet<>();
		for (String role : roles) {
			userRoles.add(roleRepository.findByRoleName(role));
		}
		return userRoles;
	}
}
