package com.common.service;

import com.common.model.Role;
import com.common.model.User;


public interface CommonService {

	void checkUser(User user);

	void checkDuplicate(String email);

	void checkAndSaveRoles(String[] roles);

	Role findRole(String roleName);

}
