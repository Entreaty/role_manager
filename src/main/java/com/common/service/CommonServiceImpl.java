package com.common.service;

import com.common.dao.RoleRepository;
import com.common.dao.UserRepository;
import com.common.exception.ApiException;
import com.common.model.Role;
import com.common.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonServiceImpl implements CommonService {

	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public void checkUser(User user) {
		if (user == null) {
			throw new ApiException("User with this id not found!");
		}
	}

	@Override
	public void checkDuplicate(String email) {
		User user = userRepository.findByEmail(email);
		if (user != null) {
			throw new ApiException(String.format("User with email '%s' already exists!", email));
		}
	}

	@Override
	public void checkAndSaveRoles(String[] roles) {
		for (String roleName : roles) {
			Role role = roleRepository.findByRole(roleName);
			if (role == null) {
				roleRepository.saveAndFlush(new Role(roleName));
			}
		}
	}

	public Role findRole(String roleName) {
		return roleRepository.findByRoleName(roleName);
	}

}
