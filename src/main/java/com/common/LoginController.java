package com.common;

import com.common.dao.UserRepository;
import com.common.model.User;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

	@Autowired
	private UserRepository userRepository;

	@ApiOperation(value = "Checking the authority", notes = "Returns user info and list of roles if exist")
	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public String check() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userRepository.findByEmail(auth.getName());
		return "Welcome " + user.getName() + " " + user.getSurname() + " (" + user.getEmail() + ")" + " " +
				String.format("Roles is = %s", user.getRoles());
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public void login(@RequestPart(value = "email", name = "email") String email,
					  @RequestPart(value = "password", name = "password") String password) {
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logout() {
	}

}
