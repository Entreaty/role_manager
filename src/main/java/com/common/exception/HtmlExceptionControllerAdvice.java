package com.common.exception;

import com.common.WebController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = WebController.class)
public class HtmlExceptionControllerAdvice {

	private static final Logger logger = LoggerFactory.getLogger(HtmlExceptionControllerAdvice.class);

	@ExceptionHandler(Exception.class)
	public String exceptionHandler(Exception ex) {
		logger.error("Web error = ", ex);
		return "500";
	}

}