package com.common.exception;

import com.common.model.ErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice(annotations = RestController.class)
public class RestExceptionControllerAdvice {

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorResponse handle(ApiException ex) {
		return createErrorResponse("com.common.exception.ApiException",
				ex.getMessage(), 404);
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handle(DataIntegrityViolationException ex) {
		return createErrorResponse("com.common.exception.ApiException",
				"This email already exists.", 400);
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Map handle(MethodArgumentNotValidException exception) {
		return error(exception.getBindingResult().getFieldErrors()
				.stream()
				.map(FieldError::getDefaultMessage)
				.collect(Collectors.toList()));
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Map handle(ConstraintViolationException exception) {
		return error(exception.getConstraintViolations()
				.stream()
				.map(ConstraintViolation::getMessage)
				.collect(Collectors.toList()));
	}

	private Map error(Object message) {
		return Collections.singletonMap("error", message);
	}

	private ErrorResponse createErrorResponse(String exception, String message, int status) {
		ErrorResponse error = new ErrorResponse();
		error.setTimestamp(System.currentTimeMillis());
		error.setStatus(status);
		error.setError("Bad Request");
		error.setException(exception);
		error.setMessage(message);
		return error;
	}
}